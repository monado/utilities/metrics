# Monado Metrics Tools

<!--
Copyright 2022, Collabora, Ltd. and the Monado contributors
SPDX-License-Identifier: BSL-1.0
-->

## Requirements

The Python version used is Python 3.

To run tools make sure to install python setuptools, protobuf, pandas,
requests and bokeh (see requirements.txt):

```bash
pip install -r requirements.txt
```

And then ensure `protoc` is accessible globally. For example,
it is can be installed and configured globally on Ubuntu with
the following command:

```bash
sudo apt-get install protobuf-compiler
```

## Build

Currently, this project supports two approaches to build and generate protobuf file.

### make

```bash
make
```

It requires the `nanopb` is located with the same level of the repository's root directory:

```txt
your_directory
   ├── metrics/
   └── nanopb/
```

[nanopb-0.4.7](https://github.com/nanopb/nanopb/releases/tag/0.4.7) is recommended.

### Python script

Build the protobuf python file with `make.py`.

```bash
python make.py
```

This scripts will download and extract `nanopb` in the directory automatically if it doesn't exist.

## Run

Run the cmd.py file and give it the protobuf file containing Monado metrics.

```bash
python cmd.py --open-plot /path/to/file.protobuf
```
