#!/bin/env python3
# Copyright 2024, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

import subprocess
import glob
import os
import shutil
import requests
import zipfile

def run_command(command):
    try:
        subprocess.run(command, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {e}")

def download_extract_nano_pb():
    if not os.path.exists('./nanopb'):
        print("nanopb folder not found. Downloading from GitHub...")
        response = requests.get('https://github.com/nanopb/nanopb/archive/refs/tags/0.4.7.zip')
        with open('nanopb.zip', 'wb') as f:
            f.write(response.content)
        with zipfile.ZipFile('nanopb.zip', 'r') as zip_ref:
            zip_ref.extractall('.')
        os.rename('nanopb-0.4.7', 'nanopb')
        os.remove('nanopb.zip')
        print("nanopb downloaded and extracted successfully.")

def main():
    proto_dir = 'proto'
    out_dir = 'out'
    python_out_dir = 'src/proto'

    # Ensure the output directories exist
    os.makedirs(out_dir, exist_ok=True)
    os.makedirs(python_out_dir, exist_ok=True)

    download_extract_nano_pb()

    # Iterate through all .proto files in the proto directory
    for proto_file in glob.glob(os.path.join(proto_dir, '*.proto')):
        print(f"Processing: {proto_file}")
        
        # Build the path for the output .pb file
        out_pb = os.path.join(out_dir, os.path.basename(proto_file).replace('.proto', '.pb'))
        
        # Use protoc to generate .pb file
        print(" :: PROTOC for .pb")
        run_command(['protoc', proto_file, '--descriptor_set_out=' + out_pb])
        
        # Use nanopb generator
        print(" :: NANOPB")
        run_command(['python', './nanopb/generator/nanopb_generator.py', '-D', out_dir, '--strip-path', out_pb])

        # Generate Python code
        print(" :: PROTOC for Python")
        run_command(['protoc', '--proto_path=' + proto_dir, '--python_out=' + python_out_dir, proto_file])

if __name__ == "__main__":
    main()
